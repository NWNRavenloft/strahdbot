const Discord = require('discord.js');

const client = new Discord.Client();

const commands = require('./commands');
const config = require('./config.json');

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', async msg => {
	const prefix = msg.content.split(" ")[0];
	if (prefix != "!sb")
		return;

	response = await commands.parse(msg.content.substring(msg.content.indexOf(" ") + 1),
		msg.author.username,
		msg.author.id
	);

	var channel = msg.channel;
	if (config.dev)
		channel = client.channels.get("468055302655311874");
	channel.send(response);
});

client.login('NDY4MDUzNjU3MjAwMzYxNTAz.DizmPg.Agbk0QFW6xPDCbR0bQCVn4lwjpA');

module.exports = {client: client}