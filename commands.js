const events = require('./events.js');

async function parse(command, author_name, author_id)
{
	var cmd_type = command.substring(0, command.indexOf(' '));

	if (cmd_type in commands)
		return await commands[cmd_type](command.substring(command.indexOf(' ')+1),
			author_name,
			author_id);
	else
		return "Error: Command not found.";
}

async function handle_cmd_roll(data, author_name, author_id)
{
	var n = 1;
	var d = 6;

	// Check for the format.
	if (data.indexOf('d') == -1)
		return "Error: Use the format **2d6**";
	if (data.indexOf('d') > 0)
		n = parseInt(data.substring(0, data.indexOf('d')));

	// Check that the left side is a number and is above 20.
	if (isNaN(n) || n > 20)
	{
		return "Error: Left side format not recognized. Use **2d6**, to maximum of **20d6**";
	}

	d = parseInt(data.substring(data.indexOf('d')+1));

	// Check that the dice used is valid and not above 100.
	if (isNaN(d) || d > 100)
	{
		return "Error: Right side format not recognized. Use **2d6**, to maximum of **2d100**";
	}

	if (n <= 0)
	{
		return "Please.";
	}

	var rolls = [];
	var total = 0;

	// Roll for n many times.
	for (var i = 0; i < n; i++)
	{
		var roll = Math.floor(Math.random() * d) + 1;
		rolls.push(roll);
		total += roll;
	}

	var formatted = "";

	// Format reply
	for (var i = 0; i < n; i++)
	{
		formatted += "**" + rolls[i] + "**  ";
	}

	if (n > 1)
		formatted += "=  " + total;

	return formatted;
}

async function handle_cmd_event(data, author_name, author_id)
{
	var sub_cmd = data.substring(0, data.indexOf(' '));
	if (sub_cmd.length <= 0)
		sub_cmd = data;
	const rest = data.substring(data.indexOf(' ')+1);

	switch (sub_cmd)
	{
		case "add":
			const [name, description, date] = rest.match(/\w+|"[^"]+"/g);

			if (!name || !description || !date)
			{
				return "Error: Command format not recognized.";
			}

			const creator = {}
			creator[author_id] = author_name;

			return await events.add(name.replace(/"/g, ''), description.replace(/"/g, ''), date.replace(/"/g, ''), creator);
		case "list":
			return JSON.stringify(await events.list());
		case "join":
			return await events.join(rest.replace(/"/g, ''), author_name, author_id);
		case "leave":
			break;
		case "delete":
			break;
	}

	return "Error: Event or event subcommand not found."
}

// Add new commands here
const commands = {
	"event": handle_cmd_event,
	"roll": handle_cmd_roll
}

module.exports = {parse: parse}