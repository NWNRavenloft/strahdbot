# StrahdBot

StrahdBot, the bot for Ravenloft: Prisoners of The Mist Discord server.

To run, clone the repository, copy config.json.example to config.json, set a
proper token string to config.json, do npm install and run node index.js.