const client = require('./discord.js').client;
const subscriber = require('./redis.js').subscriber;
const config = require('./config.json');

// This needs rework when adding more redis stuff - right now we're not checking for channel.
// move subscriber.on to redis.js.
subscriber.on("message", function(channel, message) {
	var styled_message = message;

	var channel = "253300451804577792";
	// If dev, send to Bot Test server's #general.
	if (config.dev)
		channel = "468055302655311874";

	client.channels.get(channel).send(message);
	if (message.includes("DM "))
	{
		//client.channels.get(channel).send("```\n" + channel + "> " + message + "\n```");
	}
	else
	{
		//client.channels.get(channel).send("```yaml\n" + channel + "> " + message + "\n```");
	}
});

subscriber.subscribe("nwserver.chat.shouts");