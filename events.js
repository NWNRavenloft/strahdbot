// January 19th 2019, 7:19:58 pm
const moment = require('moment');
const chrono = require('chrono-node');

// 2019-01-19T14:33:53Z
const client = require('./discord.js').client;
const config = require('./config.json');

const Sequelize = require('sequelize');
// db, user, password
const sequelize = new Sequelize('potmtest', 'potm', 'potmtest', {
	host: 'www.tzaeru.com',
	dialect: 'mysql',
	operatorsAliases: false,
	logging: false,

	pool: {
		max: 5,
		min: 0,
		acquire: 30000,
		idle: 10000
	},
});

const Event = sequelize.define('event', {
	name: {
		type: Sequelize.STRING,
		unique: true
	},
	description: {
		type: Sequelize.STRING
	},
	date: {
		type: Sequelize.DATE
	},
	participants: {
		type: Sequelize.JSON
	},
	creator: {
		type: Sequelize.JSON
	},
	announced: {
		type: Sequelize.BOOLEAN
	}
});

// force: true will drop the table if it already exists
Event.sync({force: true}).then(() => {
	// Table created
	return Event.create({
		name: 'Event',
		description: 'Description',
		date: new Date().getDate(),
		participants: {},
		creator: {"166925527016275968": "tzaeru"},
		announced: false
	});
});

async function add(name, description, date, creator)
{
	const parsed_date = chrono.parseDate(date);

	await Event.create({
		name: name,
		description: description,
		date: parsed_date,
		participants: {},
		creator: creator,
		announced: false
	});

	return "Created event " + name + ".";
}

async function list()
{
	const event = await Event.findAll();
	const events = []

	event.forEach(function(el) {
		const obj = {};
		obj.name = el.name;
		obj.description = el.description;
		obj.date = el.date;
		obj.participants = el.participants;
		obj.announced = el.announced;
		events.push(obj);
	});

	return events;
}

async function join(name, nick, id)
{
	const event = await Event.findOne({
		where: {
			name: name,
		}
	});

	event.participants[id] = nick;

	await Event.update({
		participants: event.participants,
		},{
		where: {
			name: name
		}
	});

	return "Joined event, " + event.toString();
}

async function leave(event, nick)
{
	const event = await Event.findOne({
		where: {
			name: name,
		}
	});

	if (id in event.participants)
		delete event.participants[id];
	else
		return "Error: Wasn't in the event to begin with.";

	await Event.update({
		participants: event.participants,
		},{
		where: {
			name: name
		}
	});

	return "Left event, " + event.toString();
}

async function del(event)
{

}

async function set_announced(name)
{
	await Event.update({
		announced: true,
		},{
		where: {
			name: name
		}
	});
}

// Checks for soon-to-start events every n seconds.
setInterval(async function() {
	const ev = await list();

	ev.forEach(async function(event) {
		var date_diff = event.date - new Date();

		if (date_diff/60000 < 60 && date_diff/60000 > 0 && !event.announced)
		{
			var mention = "";

			for (var key in event.participants) {
				console.log(key);
				mention += "<@" + key + "> ";
			}

			var channel = client.channels.get("468055302655311874");

			if (config.dev)
				channel = client.channels.get("468055302655311874");

			client.channels.get(channel).send("**" + event.name + "** in **one hour**!\n" + mention);
		
			await events.set_announced(event.name);
		}
	});
}, 5000);

module.exports = {add: add, join: join, leave: leave, delete: del, list: list, set_announced: set_announced}